#include "config.hpp"

params ["_logic"];
//generalParams(todo move to cfg)

//todo more definable config stuff
_twnlist = [];
 if (isnil {_logic getvariable "townlist"}) then {

	_create = nearestLocations [[0,0,0], ["CityCenter","NameCity","NameCityCapital","NameVillage"], 100000];

	_LogGrp = createGroup sideLogic;

	{
		_newLoc = _LogGrp createUnit ["LocationArea_F",position _x,[],0,"NONE"];
		_newLoc setVariable ["class",str _newloc];
		_newLoc setVariable ["name",str _newloc];
		_newLoc setVariable ["type","Area"];
    hint format ["%1",_newLoc];
		_twnlist pushBackUnique _newLoc;
	} foreach _create;

	{
		_newLoc = _x;
		_neighbors = [];

		{
			if ((_newLoc distance _x) < 2500) then {_neighbors pushBack _x};
		} foreach (_twnlist - [_newLoc]);


		_newLoc setVariable ["neighbors",_neighbors];

	} foreach _twnlist;

	_logic setvariable ["townlist",_twnlist];



};
//setup civs\vics
//_logic setvariable ["KC_CIV_classes",_poolCiv];
//_logic setvariable ["KC_CIV_classesVehicles",_poolCivVic];

{
  //Pass town,spawn distance,civTypes,civVehicles
  _handle = [_x,_spawnDistance,_poolCiv,_poolCivVic,_maxCiv] execFSM "FSM\Town_Controller.fsm";
}foreach _twnlist;
