/*
 * Author: Chris
 * [Description]
 *
 * Arguments:
 * 0: Town <LocationF Ojbect>
 * 1: House <Building Object>
 *
 * Return Value:
 * Civ created, <UNIT>
 *
 * Example:
 * _unitReturned = [_town,_house] call KC_CIV_createCiv;
 *
 * Public: [Yes/No]
 */
 #include "config.hpp"


params ["_town","_townSize","_house"];
_townPos = position _town;
_civSelected = _poolCiv call BIS_fnc_selectRandom;
_grp = _town getVariable "CIV_group";
_unit = _grp createUnit [_civSelected, _townPos, [], 0, "FORM"];
//Move unit
_unit setpos (selectRandom ([_house] call BIS_fnc_buildingPositions));
//DynSim
_unit enableDynamicSimulation true;
_unit triggerDynamicSimulation false;//Really? If this is not ran, every civ activates units that are simulated.
//Make him dumb
_unit setSkill 0;
_unit disableAI "AUTOTARGET";
_unit disableAI "TARGET";
_unit forcewalk true;
_unit setbehaviour "safe";
//Todo unit vars for home etc.
//_available_behaviours = ["walk","travel",];
//_unit setVariable ["CIV_behaviours",_available_behaviours];
_unit setVariable ["CIV_busy",false];
_unit setVariable ["CIV_Town",_town];
_unit setVariable ["CIV_Home",_house];

//Run behaviour FSM
_fsm = [_unit,_townSize,_townPos] execFSM "FSM\civ_behaviour.fsm";
_unit setVariable ["CIV_fsm",_fsm];

_unit
